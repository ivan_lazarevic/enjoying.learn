﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services;
using ExampleApp.ViewModels;
using Modles;
using Microsoft.AspNetCore.Authorization;

namespace ExampleApp.Controllers
{
    [Authorize]
    [Route("[controller]/[action]/{id?}")]
    public class HomeController : IndexBaseController
    {
        IFlightsData _flightsData;
        public HomeController(IFlightsData flightsData)
        {
            _flightsData = flightsData;
        }

        public override ViewResult Index()
        {
            var model = new HomePageViewModel();
            model.Flights = _flightsData.GetAll();
            model.Message = "controller message";

            return View(model);
        }

        public IActionResult Details(int id)
        {
            var model = _flightsData.Get(id);
            if (model == null)
            {
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FlightEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newFlight = new Flights();
                newFlight.FType = model.FType;
                newFlight.Name = model.Name;
                newFlight = _flightsData.Add(newFlight);

                return RedirectToAction("Details", new { id = newFlight.Id });
            }
            return View();
        }
    }
}
