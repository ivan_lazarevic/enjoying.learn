﻿using ExampleApp.ActionResults;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.Controllers
{
    public abstract class IndexBaseController : Controller

    {


        public IndexBaseController()
        {
        }

        //public virtual FileActionResult Index()
        //{
        //    return new FileActionResult(@"");
        //}


        public virtual ViewResult Index()
        {
            return View();
        }

        [HttpPost(Name = "Index")]
        public virtual string Index_Post()
        {
            return "IndexPost";
        }
    }
}
