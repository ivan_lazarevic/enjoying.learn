﻿/// <binding BeforeBuild='copy-assets' />
"use strict";

var gulp = require('gulp');

gulp.task('copy-files', function () {  
    gulp.src('./node_modules/bootstrap/dist/js/bootstrap.js').pipe(gulp.dest('./wwwroot/'));
    gulp.src('./node_modules/jquery/jquery.min.js').pipe(gulp.dest('./wwwroot/'));
    gulp.src('./node_modules/bootstrap/dist/css/bootstrap.css').pipe(gulp.dest('./wwwroot/'));
});