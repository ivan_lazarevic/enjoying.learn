﻿using ExampleApp.ViewModels;
using FluentValidation;
using Modles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.Validators
{
    public class FlightValidator : AbstractValidator<FlightEditViewModel>
    {
        public FlightValidator()
        {
            RuleFor(x => x.Name).NotNull().WithMessage("Name is required!").NotEmpty().WithMessage("Name is required!");


        }
    }
}
