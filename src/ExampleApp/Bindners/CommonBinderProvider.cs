﻿using ExampleApp.Bindners;
using ExampleApp.ViewModels;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.Bindners
{
    public class CommonBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context.Metadata.ModelType == typeof(FlightEditViewModel))
                return new FlightBinder();
            return null;
        }

    }
}
