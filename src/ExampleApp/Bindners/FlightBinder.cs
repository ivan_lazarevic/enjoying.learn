﻿using ExampleApp.Validators;
using ExampleApp.ViewModels;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Modles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.Bindners
{
    public class FlightBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            FlightEditViewModel flight = new FlightEditViewModel();


            ValueProviderResult idValueResult = bindingContext.ValueProvider.GetValue("Id");
            //  flight.Id = idValueResult.FirstValue;
            ValueProviderResult nameValueResult = bindingContext.ValueProvider.GetValue("Name");
            flight.Name = nameValueResult.FirstValue;

            validateBinder(bindingContext, flight);
            bindingContext.Result = ModelBindingResult.Success(flight);

            return Task.CompletedTask;
        }
        private void validateBinder(ModelBindingContext bindingContext, FlightEditViewModel flight)
        {
            var validator = new FlightValidator();
            FluentValidation.Results.ValidationResult results = validator.Validate(flight);
            results.AddToModelState(bindingContext.ModelState, null);
        }
    }
}
