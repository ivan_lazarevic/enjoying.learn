﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Modles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace ExampleApp.Auth
{
    public class CertificateAuthRequirement : IAuthorizationRequirement
    {
        public CertificateAuthRequirement()
        {
        }
    }

    public class CertificateAuthHandler : AuthorizationHandler<CertificateAuthRequirement>
    {



        public CertificateAuthHandler()
        {
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CertificateAuthRequirement requirement)
        {


            //RolesAuthorizationRequirement rolesAuthorizationRequirement = AuthHelper.ColectRoles(context);

            string certificateBase64String = ((Microsoft.AspNetCore.Http.Internal.DefaultHttpRequest)((Microsoft.AspNetCore.Http.DefaultHttpContext)((Microsoft.AspNetCore.Mvc.ActionContext)context.Resource).HttpContext).Request).Headers["MS-ASPNETCORE-CLIENTCERT"];            
            if (!string.IsNullOrWhiteSpace(certificateBase64String))
            {
                byte[] clientCertBytes = Convert.FromBase64String(certificateBase64String);
                X509Certificate2 certificate = new X509Certificate2(clientCertBytes);

            }
                //context.Succeed(rolesAuthorizationRequirement as IAuthorizationRequirement);
                var httpContext = ((Microsoft.AspNetCore.Mvc.ActionContext)context.Resource).HttpContext;
            var user = (User)httpContext.Items["CurrentUser"];

            return Task.FromResult(0);
        }
    }
}
