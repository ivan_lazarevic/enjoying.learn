﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using DataRepository;
using Modles;

namespace ExampleApp.Migrations
{
    [DbContext(typeof(FlightsDbContext))]
    [Migration("20170226175542_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Modles.Flights", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateArrival");

                    b.Property<int>("FType");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("FlightsList");
                });
        }
    }
}
