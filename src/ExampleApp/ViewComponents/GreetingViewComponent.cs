﻿using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.ViewComponents
{
    public class TestViewComponent : ViewComponent
    {
        private IConfigurationReader _configurationReader;

        public TestViewComponent(IConfigurationReader configurationReader)
        {
            _configurationReader = configurationReader;
        }

        public Task<IViewComponentResult> InvokeAsync()
        {
            var model = _configurationReader.ReadConnectionString();
            return Task.FromResult<IViewComponentResult>(View("Default", model));
        }
    }
}
