﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Services;
using Modles;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Routing;
using ExampleApp.Bindners;
using DataRepository;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using ExampleApp.Auth;

namespace ExampleApp
{
    public class Startup
    {
        IConfiguration Configuration
        {
            get; set;
        }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile("runtimesettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
            Configuration = builder.Build();
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.Configure<MyOptions>(Configuration);
            services.AddSingleton<IConfigurationReader, ConfigurationReader>();
            services.AddSingleton<IFlightsData, SqlFlightsData>();
            services.AddSingleton(provider => Configuration);
            services.AddDbContext<FlightsDbContext>(options =>
                   options.UseSqlServer(@"data source=brankoondo-pc\SQLEXPRESS;initial catalog=DemoTest;integrated security=true", b => b.MigrationsAssembly("ExampleApp")));
            services.AddIdentity<User, IdentityRole>(o =>
            {
                o.Password.RequireDigit = false;
            }).AddEntityFrameworkStores<FlightsDbContext>();

            services.AddMvc(config => config.ModelBinderProviders.Insert(0, new CommonBinderProvider()));

            services.AddAuthorization(options =>
            {
                options.AddPolicy("CertificateAuthPolicy",
                                  policy => policy.Requirements.Add(new CertificateAuthRequirement()));
            });

           
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.CookieHttpOnly = true;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IConfigurationReader configReader)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }



            var dbString = Configuration["ConnectionStrings:DefaultConnection"];

            //app.Use(async (context, next) =>
            //{
            //    await context.Response.WriteAsync("Hello World!");
            //    await next.Invoke();
            //});
            //app.Run(async (context) =>
            //{
            //    await context.Response.WriteAsync(configReader.ReadOption1());

            //});
            app.UseStaticFiles();

            // app.UseMvcWithDefaultRoute();

            app.UseIdentity();

            app.Use(async (context, next) =>
            {
                if (context.Request.IsHttps)
                {
                    await next();
                }
                else
                {
                    var httpsUrl = "https://" + context.Request.Host + context.Request.Path;
                    context.Response.Redirect(httpsUrl);
                }
            });

            app.UseMvc(ConfigureRoutes);


            app.UseWelcomePage();
        }

        private void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Default",
               "{controller=home}/{action=Index}/{id?}");

            routeBuilder.MapRoute("123",
               "{controller=home}/{action}/{id?}");
        }
    }
}
