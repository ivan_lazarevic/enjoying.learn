﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.ActionResults
{
    public class TestActionResult : IActionResult
    {
        string regularLocal = "";
        public TestActionResult(string regular)
        {
            regularLocal = regular;
        }
        public Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = 200;
            return context.HttpContext.Response.WriteAsync(regularLocal.ToUpper());
        }
    }

    public class FileActionResult : IActionResult
    {
        string _filepath = "";
        public FileActionResult(string filepath)
        {
            if(string.IsNullOrEmpty(filepath))
            {
                throw new ArgumentException("File Path Empty");
            }
            _filepath = filepath;
        }
        public Task ExecuteResultAsync(ActionContext context)
        {
            var bytes = File.ReadAllBytes(_filepath);
            context.HttpContext.Response.ContentType = "application/text";         
            context.HttpContext.Response.StatusCode = 200;
            return context.HttpContext.Response.Body.WriteAsync(bytes, 0, bytes.Length);

        }
    }

}

