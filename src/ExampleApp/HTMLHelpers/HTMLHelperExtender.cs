﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.HTMLHelpers
{
    public static class HTMLHelperExtender
    {
        public static HtmlString SubmitButton(this IHtmlHelper helper, string text)
        {
            return new HtmlString(
                string.Format(@"<button type=""submit"" class=""btn btn-large btn-primary""><i class='fa fa-lg fa-fw fa-save'></i>{0}</button>", text));
         
        }
    }
}
