﻿using Modles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.ViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<Flights> Flights { get; set; }
        public string Message { get; set; }
    }
}
