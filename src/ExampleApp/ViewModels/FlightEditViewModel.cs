﻿using Modles;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleApp.ViewModels
{
    public class FlightEditViewModel
    {

        public int Id { get; set; }
        [Required]
        [MaxLength(40,ErrorMessage = "Tekst je predugacak")]
       
        public string Name { get; set; }
        public FlighType FType { get; set; }
        public DateTime DateArrival { get; set; }

    }
}
