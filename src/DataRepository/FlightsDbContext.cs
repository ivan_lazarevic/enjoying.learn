﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Modles;

namespace DataRepository
{
    public class FlightsDbContext : IdentityDbContext<User>
    {

        public FlightsDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Flights> FlightsList { get; set; }
    }
}
