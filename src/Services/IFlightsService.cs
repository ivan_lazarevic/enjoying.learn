﻿using DataRepository;
using Modles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public interface IFlightsData
    {
        IEnumerable<Flights> GetAll();
        Flights Get(int id);
        Flights Add(Flights newFlight);
        int Commit();
    }


    public class SqlFlightsData : IFlightsData
    {
        private FlightsDbContext _context;

        public SqlFlightsData(FlightsDbContext context)
        {
            _context = context;
        }

        public Flights Add(Flights newFlight)
        {
            _context.Add(newFlight);
            _context.SaveChanges();
            return newFlight;
        }

        public Flights Get(int id)
        {
            return _context.FlightsList.FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<Flights> GetAll()
        {
            return _context.FlightsList;
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }
    }

    public class InMemoryFlightsData : IFlightsData
    {
        static InMemoryFlightsData()
        {
            _flights = new List<Flights>
            {
                new Flights { Id = 1, Name="BEG - IR" },
                new Flights { Id = 2, Name = "BEG - IP" },
                new Flights { Id = 3, Name = "BEG - IQ" }
            };
        }

        public int Commit()
        {
            return 0;
        }

        public Flights Add(Flights newFlight)
        {
            newFlight.Id = _flights.Max(r => r.Id) + 1;
            _flights.Add(newFlight);
            return newFlight;
        }

        public Flights Get(int id)
        {
            return _flights.FirstOrDefault(r => r.Id == id);
        }

        public IEnumerable<Flights> GetAll()
        {
            return _flights;
        }

        static List<Flights> _flights;
    }
}
