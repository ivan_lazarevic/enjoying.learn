﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Modles;

namespace Services
{

    public interface IConfigurationReader
    {
        string ReadConnectionString();
        string ReadOption1();

    }
    public class ConfigurationReader : IConfigurationReader
    {
        IConfiguration _configuration;

        private readonly IOptionsSnapshot<MyOptions> _optionsAccessor;

        MyOptions optionsReload;

        public ConfigurationReader(IConfiguration configuration, IOptionsSnapshot<MyOptions> optionsAccessor)
        {
            _configuration = configuration;
            _optionsAccessor = optionsAccessor;
            optionsReload = optionsAccessor.Value;
        }
        public string ReadConnectionString()
        {
            return _configuration["ConnectionStrings:DefaultConnection"];
        }

        public string ReadOption1()
        {
            return optionsReload.subsection.suboption1;
        }
    }
}
