﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Modles
{
    public partial class Flights
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public FlighType FType { get; set; }
        public DateTime DateArrival { get; set; }
    }

    [ModelMetadataType(typeof(FlightsMetadata))]
    public partial class Flights
    {
    }

    public partial class FlightsMetadata
    {
        [MaxLength(2, ErrorMessage = "Tekst je predugacak")]
        public string Name { get; set; }       

    }

    public enum FlighType
    {
        Type1,
        Type2,
        Type3
    }
}
