﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Modles
{
    public class MyOptions
    {
        public MyOptions()
        {
            // Set default value.
            Option1 = "value1";
        }
        public string Option1 { get; set; }
        public int Option2 { get; set; } = 5;
        public subsection subsection { get; set; }
    }

    public class subsection
    {
        public subsection()
        {
            // Set default values.
            suboption1 = "value1_from_ctor";           
        }
        public string suboption1 { get; set; }
    }
}
